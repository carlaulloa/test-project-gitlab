**Description**

Descripción detallada del issue.

**Acceptance Test**

¿Qué comportamientos debe tener el software para que este issue se considere como completado o aceptable?

**Implementation Notes**

Cualquier detalle ajeno a esta implementación en específico.

**Planning Estimate**

X horas/días.

**References**

URLs de apoyo o notas sobre si esta issue se relaciona con otro (dependencia, etc).
